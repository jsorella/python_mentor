import os
import fnmatch
import sys


class FilesTool(object):

    def files_with(self, needle):
        """
        Returns a list of text files that has a certain string.
        :param needle:      String to search inside files.
        :return:            A list of file paths.
        """
        return [x for x in self._text_file_paths()
                if self._file_has_string(x, needle)]

    def _text_file_paths(self):
        """
        Returns a list of txt files inside the local path.
        :return: A list of txt file paths.
        """
        text_files = []

        for root, dirnames, filenames in os.walk('.'):
            for filename in fnmatch.filter(filenames, '*.txt'):
                text_files.append(os.path.join(root, filename))

        return text_files

    def _file_has_string(self, file_path, needle):
        """
        Determines if substring exist in a file content.
        :param file_path:   File path.
        :param needle:      String to search.
        :return: Boolean.
        """
        with open(file_path) as file_content:
            try:
                return needle in file_content.read()
            except IOError:
                raise Exception("Could not read file: " + file_path)


if __name__ == "__main__":

    if len(sys.argv) != 2:
        raise Exception('Incorrect number of arguments.\n'
                        'Usage:\n'
                        '  content_file_search.py <needle>')

    needle = sys.argv[1]
    tool = FilesTool()

    for x in tool.files_with(needle):
        print x
