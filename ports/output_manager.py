from abc import ABCMeta, abstractmethod


class OutputManager:
    def __init__(self, cli_address):
        self.cli_address = cli_address

    @abstractmethod
    def send(self, data):
        pass


class TerminalManager(OutputManager):

    def send(self, data):
        print "Message from {0}:{1}>".format(
            self.cli_address[0], self.cli_address[1]
        )

        try:
            data.decode('utf-8')

            if len(data) <= 60:
                print data
            else:
                print data[:60] + '...'

        except UnicodeError:
            bytes = [x for x in bytearray(data)[:30]]
            bytes.append('...')
            print "Bytes: {0}".format(bytes)


class FileManager(OutputManager):
    file_path = 'output'

    def send(self, data):
        try:
            with open(self.file_path, 'wb') as file:
                file.write(data)
        except IOError:
            raise Exception("Could not write file: " + self.file_path)
