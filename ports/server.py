import socket
import ConfigParser
from output_manager import TerminalManager, FileManager


def run():
    config = get_config_params()

    socket_s = socket.socket()
    socket_s.bind((config['host'], config['port']))
    socket_s.listen(config['max_connections'])

    print("Server started - Awaiting connections...")

    socket_c, cli_address = socket_s.accept()

    print("Connection accepted from {0}:{1}".format(cli_address[0],
                                                    cli_address[1]))

    manage_input_data(
        socket_c, cli_address, config['max_bytes'], config['managers']
    )

    print("Server Closing...")

    socket_s.close()
    socket_c.close()


def get_config_params():
    config = ConfigParser.RawConfigParser()
    config.read('server.cfg')

    return {
        'host': config.get('Connection', 'Host'),
        'port': config.getint('Connection', 'Port'),
        'max_connections': config.getint('Preferences', 'MaxConnections'),
        'max_bytes': config.getint('Preferences', 'MaxBytes'),
        'managers': config.get('Preferences', 'OutputManagers')
    }


def manage_input_data(socket, cli_addr, max_bytes, managers):
    m_names = [chunk.strip() for chunk in managers.split(',')]
    m_dict = get_managers_dict()

    output_managers = [m_dict[name](cli_addr) for name in m_names]

    received = receive_data(socket, max_bytes)

    for manager in output_managers:
        manager.send(received)

    socket.send(received)


def get_managers_dict():
    return {
        'terminal': TerminalManager,
        'file': FileManager
    }


def receive_data(socket, max_bytes):
    chunks = []

    while True:
        chunk = socket.recv(max_bytes)
        if chunk:
            chunks.append(chunk)
        else:
            break

    return ''.join(chunks)

if __name__ == "__main__":
    run()
