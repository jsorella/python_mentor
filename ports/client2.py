import socket
import ConfigParser


config = ConfigParser.RawConfigParser()
config.read('client.cfg')

host = config.get('Connection', 'Host')
port = config.getint('Connection', 'Port')

s = socket.socket()
s.connect((host, port))

print "Client successfully connected to server through " \
      "port {0}".format(s.getsockname()[1])

with open('cat.jpg', 'rb') as file:
    message = file.read()

s.send(message)

print "Closing client application..."
s.close()
